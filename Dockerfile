FROM mykro/java8-jre as build 
COPY . /usr/app
WORKDIR /usr/app
RUN ./gradlew build

FROM openjdk:8-jre-alpine
# Create a group and user
RUN addgroup -S gradle && adduser -S gradle -G gradle
COPY --from=build /usr/app/ /home/gradle/app/
USER gradle
WORKDIR /home/gradle/app

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "build/libs/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar"]
